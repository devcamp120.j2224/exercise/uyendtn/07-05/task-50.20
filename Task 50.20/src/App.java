import com.devcamp.j50_javabasjc.s10.NewDevcampApp;

public class App {
    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        //This is a comment
        String appName = "Devcamp will help everyone know coding";
        System.out.println("hello world " + appName.length());
        System.out.println("Uppercase " + appName.toUpperCase());//this is a method
        System.out.println("lowerCase " + appName.toLowerCase());//this is a method
        NewDevcampApp.name("Ngoc Uyen", 30);
        NewDevcampApp newApp = new NewDevcampApp();
        newApp.name("Dang Thi Ngoc Uyen");
    }
}
